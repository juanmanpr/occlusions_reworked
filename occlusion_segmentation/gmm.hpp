#ifndef _GMM_HPP_
#define _GMM_HPP_

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv/cv.hpp>
#include <limits>
#include <map>

using namespace cv;

class GMM
{
public:
    static const int componentsCount = 2;

    GMM( Mat& _model );
    double operator()( const Vec3d color ) const;
    double operator()( int ci, const Vec3d color ) const;
    int whichComponent( const Vec3d color ) const;

    void initLearning();
    void addSample( int ci, const Vec3d color );
    void endLearning();

private:
    void calcInverseCovAndDeterm( int ci );
    Mat model;
    double* coefs;
    double* mean;
    double* cov;

    double inverseCovs[componentsCount][3][3];
    double covDeterms[componentsCount];

    double sums[componentsCount][3];
    double prods[componentsCount][3][3];
    int sampleCounts[componentsCount];
    int totalSampleCount;
};

void initGMMs( const Mat& img, const std::vector<Point>& points, GMM& gmm );
void evalGMMs( const Mat& img, const std::vector<Point>& points, const GMM& gmm, cv::Mat& out );
void evalGMMs( const Vec3b& color, const GMM& gmm, float& out );
void trainGmmForPixelsets( const cv::Mat& locally_filtered,
                           std::map<int, std::vector< cv::Point > >& superpixels,
                           std::vector<GMM>& local_models );
cv::Mat evalImage(const cv::Mat& img, std::map<int, std::vector< cv::Point > >& superpixels, std::vector<GMM>& local_models);

#endif

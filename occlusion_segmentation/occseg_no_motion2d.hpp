
#ifndef OCCSEG_HPP
#define OCCSEG_HPP

// String
#include <string>

// Opencv
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/optflow.hpp>
#include "../flow/colorcode.h"

// GCO
#include "../gco/GCoptimization.h"

// GMM
#include "gmm.hpp"

/*!
 * \brief The OcclusionSegmentation class
 */
class OcclusionSegmentation{
public:

    /*!
     * \brief The Params struct
     */
    struct Params
    {
        explicit Params(const float beta_gmm = 10.0f,
                        const float beta_flow = 10.0f,
                        const float beta_rgb = 10.0f,
                        const float epsilon = 1.0f,
                        const float alpha_rgb = 1.0f,
                        const float alpha_gra = 1.0f,
                        const float potts = 1.0f)
        {
            this->beta_gmm = beta_gmm;
            this->beta_flow = beta_flow;
            this->beta_rgb = beta_rgb;
            this->epsilon = epsilon;
            this->alpha_gra = alpha_gra;
            this->alpha_rgb = alpha_rgb;
            this->potts = potts;
        }

        float beta_gmm;
        float beta_rgb;
        float beta_flow;
        float epsilon;
        float alpha_rgb;
        float alpha_gra;
        float potts;
    };

    OcclusionSegmentation(const OcclusionSegmentation::Params &params = OcclusionSegmentation::Params());
    ~OcclusionSegmentation();

    void compute(const cv::Mat im0, const cv::Mat im1, const cv::Mat flow_f = cv::Mat(), const cv::Mat flow_b = cv::Mat());
    cv::Mat getOcclusions() const;
private:
    OcclusionSegmentation::Params params;
    cv::Mat i_0, i_1;
    cv::Mat h_0, h_1;
    cv::Mat gra;
    cv::Mat flow_f, flow_b;
    cv::Mat occlusion_map;
    cv::Mat gmm_likelihoodmap;
    cv::Mat flo_differencemap;
    cv::Mat col_differencemap;

    cv::Mat computeFlow(const cv::Mat gr0, const cv::Mat gr1);
    cv::Mat computeGmmLikelihoodMap();
    cv::Mat computeFloDifferenceMap();
    cv::Mat computeColDifferenceMap();

    cv::Mat solveMRF();
};

class BilateralFilter {
public:
    BilateralFilter(int radius, double sigma_color, double sigma_space):
        m_sigma_color(sigma_color),m_sigma_space(sigma_space),m_radius(radius){}

    void filt( const cv::Mat& imIn, cv::Mat& imOut );
    // filt with flow (trilateral filter)
    void filtWithFlow( const cv::Mat& imIn, const cv::Mat& flow, cv::Mat& imOut, float sigmaFlow );

private:
    float m_sigma_color;
    float m_sigma_space;
    int m_radius;
};

class XBilateralFilter {
public:
    XBilateralFilter(int radius, double sigma_color, double sigma_space):
        m_sigma_color(sigma_color),m_sigma_space(sigma_space),m_radius(radius){}

    // normal cross filtering
    void filt( const cv::Mat& im1, const cv::Mat& im2, const cv::Mat& flow, cv::Mat& imOut );
    // using same weights (structure maintaining)
    void filt2( const cv::Mat& im1, const cv::Mat& im2, const cv::Mat& flow, cv::Mat& imOut );
    //
    void filt3( const cv::Mat& im1, const cv::Mat& im2, const cv::Mat& flow, cv::Mat& imOut );
    // filt2 with flow ( trilateral)
    void filtWithFlow( const cv::Mat& im1, const cv::Mat& im2, const cv::Mat& flow, cv::Mat& imOut, float sigmaFlow );

private:
    float m_sigma_color;
    float m_sigma_space;
    int m_radius;
};

#endif

#ifndef SLIC_OPENCV_HPP
#define SLIC_OPENCV_HPP

#include <opencv2/core.hpp>
#include <map>
#include <vector>
#include "SLIC/SLICO.h"
#include "SLIC/SLIC.h"
#include <iostream>

namespace cv{

class Slic{

public:
    /*!
        @brief constructor of the slic class
        @param numK, approximate number of sueprpixels
        @param m, compactness parameter, the higher the more compact. If its smaller than 0, the SLICO algorithm is used.
    */
    Slic(int _numK, int _m ):numK(_numK),m(_m),run(false){}
    Slic( ):numK(100),m(10),run(false){}

    virtual ~Slic();

    /*!
        @brief compute superpixels with the SLIC algorithm
        @param const cv::Mat& im, input image
        @param std::map<int, std::vector< cv::Point > >& superpixels, output map of superpixels
    */
    void findSLICSuperPixels( const cv::Mat& im, std::map<int, std::vector< cv::Point > >& superpixels, cv::Mat& superpixels_mat );
    void findSLICSuperPixels( const cv::Mat& im, std::map<int, std::vector< cv::Point > >& superpixels );
	void findSuperVoxels( const std::vector< cv::Mat >& ims, std::vector<cv::Mat>& supervoxels, std::vector< cv::Mat >& draws );
    void drawSLICSuperPixels( const cv::Mat& im, cv::Mat& out );

	static void drawContours( const cv::Mat& im, const cv::Mat& mask, cv::Mat& out );
private:
	int numK, m,r,c;
    SLICO oslic;
    SLIC sslic;
	bool run;
	int *labs;
};

} // cv

#endif

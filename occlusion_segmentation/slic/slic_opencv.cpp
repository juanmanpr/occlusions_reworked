
#include "slic_opencv.hpp"

namespace cv{

Slic::~Slic(){
    if (labs==NULL)
        delete [] labs;
}

static void mat2uints( const cv::Mat& in, unsigned int *out ){
	
	int idx=0;
	for ( int i=0; i<in.rows; ++i ){
		for ( int j=0; j<in.cols; ++j ){
			cv::Vec3b buf = in.at<cv::Vec3b>(i,j); 
			out[idx++] = ((unsigned int)buf[0] << 16) | ((unsigned int)buf[1] << 8) | ((unsigned int)buf[2]);
		}
	}
}

static void uints2mat( unsigned int *in, cv::Mat& out ){

	int idx=0;
	for ( int i=0; i<out.rows; ++i ){
		for ( int j=0; j<out.cols; ++j ){
			cv::Vec3b buf( uchar((in[idx] >> 16) & 0xFF), (in[idx] >> 8) & 0xFF, (in[idx]) & 0xFF );
			out.at<cv::Vec3b>(i,j) = buf;
			idx++;
		}
	}
}

static void buildmap( int *in, int rows, int cols, std::map<int, std::vector< cv::Point > >& superpixels ){
	int idx=0;
	for ( int i=0; i<rows; ++i ){
		for ( int j=0; j<cols; ++j ){
			std::vector< cv::Point > *vec = &superpixels[ in[idx++] ];
			vec->push_back( cv::Point(j,i) );
		}
	}	
}

static void buildmap( int *in, int rows, int cols, cv::Mat& superpixels ){
    superpixels = cv::Mat::zeros( rows, cols, CV_32SC1 );
    int idx=0;
    for ( int i=0; i<rows; ++i ){
        for ( int j=0; j<cols; ++j ){
            superpixels.at<int>(i,j) = in[idx++];
        }
    }
}

void Slic::findSuperVoxels( const std::vector< cv::Mat >& ims, std::vector<cv::Mat>& supervoxels, std::vector< cv::Mat >& draws ) {
	CV_Assert( ims.size()>0 );
	SLIC slicer;
	labs = new int[this->c*this->r];

	const int width	= ims[0].cols;
    const int height = ims[0].rows;
    const int depth = int(ims.size());
    const int sz = (width*height);
	int supervoxelsize = 8;
	double compactness = 20.0;
	int numlabels = 0;

	int** klabels = new int*[depth];
    unsigned int** ubuff = new unsigned int *[depth];
    for(int i=0; i < depth; ++i) {
            ubuff[i] = new unsigned int[sz];
            klabels[i] = new int[sz];

			int k=0;
			for ( int r=0; r<ims[i].rows; ++r ){
				for ( int c=0; c<ims[i].cols; ++c ){
					cv::Vec3b buf = ims[i].at<cv::Vec3b>(r,c); 
                    ubuff[i][k++] = ((unsigned int)buf[0] << 16) | ((unsigned int)buf[1] << 8) | ((unsigned int)buf[2]);
				}
			}
    }

	slicer.DoSupervoxelSegmentation( ubuff, width, height, depth, klabels, numlabels, supervoxelsize, compactness );

	draws.resize(depth);
	for(int i=0; i < depth; ++i) {
		unsigned int *buff = new unsigned int[sz];
		int* labs = new int[sz];
		int k=0;
		int maxi=0;
		for ( int r=0; r<ims[i].rows; ++r ){
			for ( int c=0; c<ims[i].cols; ++c ){
				cv::Vec3b buf = ims[i].at<cv::Vec3b>(r,c); 
				buff[k] = ((unsigned int)buf[0] << 16) | ((unsigned int)buf[1] << 8) | ((unsigned int)buf[2]);
				labs[k] = klabels[i][k];
				if (maxi<klabels[i][k]) maxi=klabels[i][k];
				k++;
			}
		}
		std::cout<<"labes = "<<maxi<<std::endl;
		slicer.DrawContoursAroundSegments( buff, labs, width, height, 12500 );
		draws[i] = cv::Mat( height, width, ims[0].type() );
		uints2mat( buff, draws[i] );

		delete [] buff;
		delete [] labs;
	}

	// map
	supervoxels.resize( 2 );
	for(int i=0; i < depth; ++i) {
		supervoxels[i] = cv::Mat( height, width, CV_32SC1 );
		int idx=0;
		for ( int r=0; r<ims[0].rows; ++r ){
			for ( int c=0; c<ims[0].cols; ++c ){
				supervoxels[i].at<int>(r,c) = klabels[i][idx++];
			}
		}
	}

	for(int i=0; i < depth; i++) {
		delete [] klabels[i];
		delete [] ubuff[i];
	}
	delete [] klabels; delete [] ubuff;
}

void Slic::findSLICSuperPixels( const cv::Mat& im, std::map<int, std::vector< cv::Point > >& superpixels ){

    unsigned int *ibuff = new unsigned int[im.cols*im.rows];

    if (run){
        delete [] labs;
    }
    this->r = im.rows;
    this->c = im.cols;
    labs = new int[this->c*this->r];
    mat2uints( im, ibuff );

    int numl=0;
    if (this->m > 0)
        sslic.DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels( ibuff, this->c, this->r, labs, numl, this->numK, this->m);
    else
        oslic.PerformSLICO_ForGivenK( ibuff, this->c, this->r, labs, numl, this->numK, this->m );
    superpixels.clear();
    buildmap( labs, im.rows, im.cols, superpixels );

    delete [] ibuff;
    run = true;
}

void Slic::findSLICSuperPixels( const cv::Mat& im, std::map<int, std::vector< cv::Point > >& superpixels, cv::Mat& superpixels_mat ){

    unsigned int *ibuff = new unsigned int[im.cols*im.rows];

    if (run){
        delete [] labs;
    }
    this->r = im.rows;
    this->c = im.cols;
    labs = new int[this->c*this->r];
    mat2uints( im, ibuff );

    int numl=0;
    if (this->m > 0)
        sslic.DoSuperpixelSegmentation_ForGivenNumberOfSuperpixels( ibuff, this->c, this->r, labs, numl, this->numK, this->m);
    else
        oslic.PerformSLICO_ForGivenK( ibuff, this->c, this->r, labs, numl, this->numK, this->m );
    superpixels.clear();
    buildmap( labs, im.rows, im.cols, superpixels );
    buildmap( labs, im.rows, im.cols, superpixels_mat );

    delete [] ibuff;
    run = true;
}

void Slic::drawSLICSuperPixels( const cv::Mat& im, cv::Mat& out ){
    out = cv::Mat(this->r, this->c, im.type());
    unsigned int *buff = new unsigned int[im.cols*im.rows];
    mat2uints( im, buff );
    if (this->m > 0)
        sslic.DrawContoursAroundSegments( buff, labs, im.cols, im.rows, 12500 );
    else
        oslic.DrawContoursAroundSegments( buff, labs, im.cols, im.rows, 12500 );
    uints2mat( buff, out );
}

static void mask2ints( const cv::Mat& in, int *out ){
	
	int idx=0;
	for ( int i=0; i<in.rows; ++i ){
		for ( int j=0; j<in.cols; ++j ){
			if ( in.at<uchar>(i,j)==255 ) {
				out[idx++] = 1;
			}else{
				out[idx++] = 0; 
			}
		}
	}
}

void Slic::drawContours( const cv::Mat& im, const cv::Mat& mask, cv::Mat& out ){
	SLICO newslic;
	out = im.clone();
	unsigned int *buff = new unsigned int[im.cols*im.rows];
	mat2uints( out, buff );
	int *outp = new int[im.cols*im.rows];
	mask2ints( mask, outp );
	
	newslic.DrawContoursAroundSegments( buff, outp, im.cols, im.rows, 12500 );
	uints2mat( buff, out );

	delete [] outp;
	delete [] buff;
}

} //cv

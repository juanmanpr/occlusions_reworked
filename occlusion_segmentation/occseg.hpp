
#ifndef OCCSEG_HPP
#define OCCSEG_HPP

// String
#include <string>

// Opencv
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/optflow.hpp>

// Include for Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DEstimator.h>
#include <CMotion2DPyramid.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>
#include <CReader.h>
#include <CImageReader.h>
#include <CMpeg2Reader.h>
#include <CImageWriter.h>
#include <FieldVector.h>


#include <opencv2/highgui.hpp>
#include "../flow/colorcode.h"

// GCO
#include "../gco/GCoptimization.h"

// DeepMatch
#include "cvdm.hpp"

struct ParamWindow{

    ParamWindow( cv::Rect w_ ){ w = w_; }
    ParamWindow( cv::Rect w_, const cv::Mat& m_ ){ w = w_; m = m_.clone(); }


    void computeFlowField( cv::Size flowsize ) {

        flow = cv::Mat::zeros(flowsize, CV_32FC2);

        for (int x=0; x<flow.cols; ++x) {
            for (int y=0; y<flow.rows; ++y) {
                flow.at<cv::Vec2f>(y,x) = cv::Vec2f( m.at<double>(0,0)*double(x)+ m.at<double>(0,1)*double(y) + m.at<double>(0,2) + m.at<double>(0,3)*double(x*x)+m.at<double>(0,4)*double(x*y)+m.at<double>(0,5)*double(y*y),
                                                     m.at<double>(1,0)*double(x)+ m.at<double>(1,1)*double(y) + m.at<double>(1,2) + m.at<double>(1,3)*double(x*x)+m.at<double>(1,4)*double(x*y)+m.at<double>(1,5)*double(y*y) );
            }
        }
    }


    cv::Rect w;
    cv::Mat m;
    cv::Mat flow;
};


/*!
 * \brief The Motion2D class is an OpenCV wrapper for the Motion2D Irisa's code.
 */
class Motion2D{

public:

    typedef CMotion2DModel Model;


    Motion2D( std::string model_id, bool var_light, int pyr_nlevels,
        int pyr_stop_level ):
        m_model_id(model_id),
        m_var_light(var_light),
        m_pyr_nlevels(pyr_nlevels),
        m_pyr_stop_level(pyr_stop_level)
    {
        // Set estimator params
        m_estimator.setFirstEstimationLevel(m_pyr_nlevels - 1);
        m_estimator.setLastEstimationLevel(m_pyr_stop_level);
        m_estimator.setRobustEstimator(true);
        m_estimator.setNIterationMaxIRLS(8);
        m_estimator.setNIterationMaxLevel(8);
        m_estimator.setRobustFunction(CMotion2DEstimator::Tukey);
    }

    Motion2D( std::string model_id, bool var_light,
              uint pyr_levels )
    {
        Motion2D(model_id, var_light, pyr_levels, 0);
    }
    Motion2D(  )
    {
        *this = Motion2D("AC", false, 3, 0);
    }
    virtual ~Motion2D();

    /*!
     * \brief computeModelAtWindow
     * \param im1
     * \param im2
     * \param window
     * \return
     */
    void computeModelAtWindow( const cv::Mat& im1, const cv::Mat& im2, cv::Rect window, cv::Mat& model, const cv::Mat& application_mask, bool use_model_initialization = false );
    void computeFlowField(cv::Mat& flow, cv::Size flowsize );

private:
    static const uchar m_label = 255;	// Value of the motion estimator support
    std::string m_model_id;	            // Parametric motion model ID to estimate
    bool m_var_light;        	        // Lighting variation parameter estimation
    int m_pyr_nlevels;                  // Number of levels in a multi-resolution pyr
    int m_pyr_stop_level;               // Pyramid level where the estimator stops
    CMotion2DEstimator m_estimator;     // Motion estimator (not to be confused with an m-estimator)
    Motion2D::Model m_motionModel;      // The computed motion model is stored in case the flow is requested
};

/*!
 * \brief The MotionCandidatesGenerator class
 */
class MotionCandidatesGenerator{
public:
    MotionCandidatesGenerator( int numLevels = 0, cv::Size baseSize = cv::Size(0,0) ):m_numLevels(numLevels), m_baseSize(baseSize)
    {
        m_motionEngine = Motion2D( "AC", false, 3, 0 );
    }
    void buildMotionCandidates( const cv::Mat& im1, const cv::Mat& im2, std::vector< cv::Mat > & motionCandidates, const cv::Mat& applicationMask );
    void buildParametricMotionWindows( const cv::Mat& im1, const cv::Mat& im2, std::vector< ParamWindow > & motionCandidates, const cv::Mat& applicationMask );

    void buildParametricMotionWindowsStaringFromMatches(
            const cv::Mat& im1, const cv::Mat& im2,
            std::vector< ParamWindow > & motionCandidates,
            const cv::Mat& applicationMask,
            const std::vector<cv::Point>& pts1,
            const std::vector<cv::Point>& pts2
            );
private:
    int m_numLevels;
    cv::Size m_baseSize;
    Motion2D m_motionEngine;

    void generate2( std::vector< std::vector< cv::Rect > > & windows );
    void generate( std::vector< std::vector< cv::Rect > > & windows );
};


class OcclusionSegmentation{
public:
    OcclusionSegmentation( const cv::Mat& _i_0, const cv::Mat& _i_1, int numWindowLevels_ = 3, bool use_deep_matches = true );
    ~OcclusionSegmentation();

    void compute_labelsfixed(cv::Mat& occlusions, double epsilon = 5.0, double coef_o = 12.5, double beta_o = 0.1 );
    void compute_occlusionfixed(cv::Mat& fusedIndexes, cv::Mat& flowVectors, double mu = 16.0, double coef_l = 12.5, double beta_l = 0.1, double label_cost = 100.0 );
    void compute(cv::Mat& fusedIndexes, cv::Mat& flowVectors, cv::Mat& occlusions, double epsilon = 5.0, double mu = 16.0,
                 double coef_l = 12.5, double coef_o = 12.5, double beta_l = 0.1, double beta_o = 0.1, double label_cost = 100.0 , int num_max_iters = 10);
    void getConstants( float& mu_robust_penalty, float& v_student_t ) {
        mu_robust_penalty = m_mu_robust_penalty;
        v_student_t = m_v_student_t;
    }

private:
    bool m_thereIsCurrentFlow;
    bool m_thereIsCurrentFlowGT;
    cv::Mat i_0, i_1, h_0, h_1, gx_0, gx_1, gy_0, gy_1, occlusion_map, models_indexes;
    cv::Mat currentFlow;
    cv::Mat currentFlowGT;
    std::vector<cv::Point> m_pts1, m_pts2;
    MotionCandidatesGenerator motGen;
    std::vector< ParamWindow > motion_labels;
    int m_numNodes;
    float m_mu_robust_penalty;
    float m_v_student_t;
    int numWindowLevels;
};

#endif

#include "occseg_no_motion2d.hpp"

#include <opencv2/optflow.hpp>
#include <opencv2/video.hpp>

#include <map>

#include "slic/slic_opencv.hpp"

namespace {
    void RectToMat( cv::Rect window, cv::Mat& labelMat, cv::Size imsize, int label, const cv::Mat& applicationMask ) {
        labelMat = cv::Mat::zeros( imsize, CV_8UC1 );
        cv::rectangle(labelMat, window, cv::Scalar(label), cv::FILLED);
        if (!applicationMask.empty())
            cv::bitwise_and( 255-applicationMask, labelMat, labelMat );
    }

    cv::Vec3f getSubpixColor3f(const cv::Mat& img, cv::Point2f pt) {
        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   img.cols, cv::BORDER_REPLICATE);
        int x1 = cv::borderInterpolate(x+1, img.cols, cv::BORDER_REPLICATE);
        int y0 = cv::borderInterpolate(y,   img.rows, cv::BORDER_REPLICATE);
        int y1 = cv::borderInterpolate(y+1, img.rows, cv::BORDER_REPLICATE);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float b = (img.at<cv::Vec3f>(y0, x0)[0] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[0] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[0] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[0] * a) * c;
        float g = (img.at<cv::Vec3f>(y0, x0)[1] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[1] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[1] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[1] * a) * c;
        float r = (img.at<cv::Vec3f>(y0, x0)[2] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[2] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[2] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[2] * a) * c;

        return cv::Vec3f(b, g, r);
    }

    cv::Vec3b getSubpixColor(const cv::Mat& img, cv::Point2f pt) {
        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   img.cols, cv::BORDER_REPLICATE);
        int x1 = cv::borderInterpolate(x+1, img.cols, cv::BORDER_REPLICATE);
        int y0 = cv::borderInterpolate(y,   img.rows, cv::BORDER_REPLICATE);
        int y1 = cv::borderInterpolate(y+1, img.rows, cv::BORDER_REPLICATE);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        unsigned char b = (img.at<cv::Vec3b>(y0, x0)[0] * (1.f - a) + img.at<cv::Vec3b>(y0, x1)[0] * a) * (1.f - c)
                               + (img.at<cv::Vec3b>(y1, x0)[0] * (1.f - a) + img.at<cv::Vec3b>(y1, x1)[0] * a) * c;
        unsigned char g = (img.at<cv::Vec3b>(y0, x0)[1] * (1.f - a) + img.at<cv::Vec3b>(y0, x1)[1] * a) * (1.f - c)
                               + (img.at<cv::Vec3b>(y1, x0)[1] * (1.f - a) + img.at<cv::Vec3b>(y1, x1)[1] * a) * c;
        unsigned char r = (img.at<cv::Vec3b>(y0, x0)[2] * (1.f - a) + img.at<cv::Vec3b>(y0, x1)[2] * a) * (1.f - c)
                               + (img.at<cv::Vec3b>(y1, x0)[2] * (1.f - a) + img.at<cv::Vec3b>(y1, x1)[2] * a) * c;

        return cv::Vec3b(b, g, r);
    }

    void clipMat( const cv::Mat& flow, cv::Mat& likelihoodMat, double mini=0.0, double maxi=255.0, bool reverse = false ){

        double maxgf, mingf;
        Mat gfdif = likelihoodMat;
        cv::minMaxIdx(gfdif,&mingf, &maxgf);

        for (int i=0; i<gfdif.cols; ++i) {
            for (int j=0; j<gfdif.rows; ++j) {

                if (!reverse) {
                    if ((i+flow.at<cv::Vec2f>(j,i)[0] < 0) ||
                        (j+flow.at<cv::Vec2f>(j,i)[1] < 0) ||
                        (i+flow.at<cv::Vec2f>(j,i)[0] >= gfdif.cols) ||
                        (j+flow.at<cv::Vec2f>(j,i)[1] >= gfdif.rows) ) {
                        gfdif.at<double>(j,i) = maxgf;
                    }

                    if ( gfdif.at<double>(j,i)>=maxi )
                        gfdif.at<double>(j,i)=maxi;
                    else if ( gfdif.at<double>(j,i)<mini )
                        gfdif.at<double>(j,i)=mini;
                }else{
                    if ((i+flow.at<cv::Vec2f>(j,i)[0] < 0) ||
                        (j+flow.at<cv::Vec2f>(j,i)[1] < 0) ||
                        (i+flow.at<cv::Vec2f>(j,i)[0] >= gfdif.cols) ||
                        (j+flow.at<cv::Vec2f>(j,i)[1] >= gfdif.rows) ) {
                        gfdif.at<double>(j,i) = mingf;
                    }

                    if ( gfdif.at<double>(j,i)>=maxi )
                        gfdif.at<double>(j,i)=maxi;
                    else if ( gfdif.at<double>(j,i)<mini )
                        gfdif.at<double>(j,i)=mini;
                }
            }
        }
    }

    cv::Vec2f getSubpixGradient(const cv::Mat& gx, const cv::Mat& gy, cv::Point2f pt) {
        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   gx.cols, cv::BORDER_REPLICATE);
        int x1 = cv::borderInterpolate(x+1, gx.cols, cv::BORDER_REPLICATE);
        int y0 = cv::borderInterpolate(y,   gx.rows, cv::BORDER_REPLICATE);
        int y1 = cv::borderInterpolate(y+1, gx.rows, cv::BORDER_REPLICATE);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float g0 = (gx.at<float>(y0, x0) * (1.f - a) + gx.at<float>(y0, x1) * a) * (1.f - c)
                               + (gx.at<float>(y1, x0) * (1.f - a) + gx.at<float>(y1, x1) * a) * c;
        float g1 = (gy.at<float>(y0, x0) * (1.f - a) + gy.at<float>(y0, x1) * a) * (1.f - c)
                               + (gy.at<float>(y1, x0) * (1.f - a) + gy.at<float>(y1, x1) * a) * c;
        return cv::Vec2f(g0, g1);
    }

    cv::Vec2f getSubpixFlow(const cv::Mat& flow, cv::Point2f pt) {
        CV_Assert(!flow.empty());
        CV_Assert(flow.channels() == 2);

        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   flow.cols, cv::BORDER_REFLECT_101);
        int x1 = cv::borderInterpolate(x+1, flow.cols, cv::BORDER_REFLECT_101);
        int y0 = cv::borderInterpolate(y,   flow.rows, cv::BORDER_REFLECT_101);
        int y1 = cv::borderInterpolate(y+1, flow.rows, cv::BORDER_REFLECT_101);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float u = (flow.at<cv::Vec2f>(y0, x0)[0] * (1.f - a) + flow.at<cv::Vec2f>(y0, x1)[0] * a) * (1.f - c)
                               + (flow.at<cv::Vec2f>(y1, x0)[0] * (1.f - a) + flow.at<cv::Vec2f>(y1, x1)[0] * a) * c;
        float v = (flow.at<cv::Vec2f>(y0, x0)[1] * (1.f - a) + flow.at<cv::Vec2f>(y0, x1)[1] * a) * (1.f - c)
                               + (flow.at<cv::Vec2f>(y1, x0)[1] * (1.f - a) + flow.at<cv::Vec2f>(y1, x1)[1] * a) * c;

        return cv::Vec2f(u, v);
    }

    static cv::Mat bwfwOcclusionMap( const cv::Mat& i_0, const cv::Mat& i_1, float epsilon_v ) {

        cv::Ptr< cv::DenseOpticalFlow > flow_estimator = cv::optflow::createOptFlow_DeepFlow();
        cv::Mat ff, bf, g0, g1;

        cv::cvtColor(i_0, g0, cv::COLOR_BGR2GRAY);
        cv::cvtColor(i_1, g1, cv::COLOR_BGR2GRAY);
        flow_estimator->calc( g0, g1, ff );
        flow_estimator->calc( g1, g0, bf );

        cv::Mat occ(i_0.size(),CV_8UC1);
        for ( int y=0; y<ff.rows; ++y ) {
            for ( int x=0; x<ff.cols; ++x ) {
                cv::Vec2f ffv = ff.at<cv::Vec2f>(y,x);
                cv::Vec2f bfv = getSubpixFlow(bf,cv::Point2f( float(x)+ffv[0], float(y)+ffv[1] ));
                float normf = cv::norm(ffv+bfv, cv::NORM_L2);
                occ.at<uchar>(y,x) = (normf<epsilon_v)?0:1;
            }
        }
        return occ;
    }

    void _bilateralFilter( const Mat& src, Mat& dst, int d,
        double sigma_color, double sigma_space) {
        BilateralFilter filter(d/2, sigma_color, sigma_space);
        filter.filt( src, dst );
    }

    void _xbilateralFilter( const Mat& src1, const Mat& src2, const Mat& flow, Mat& dst, int d,
        double sigma_color, double sigma_space) {
        XBilateralFilter filter(d/2, sigma_color, sigma_space);
        filter.filt2( src1, src2, flow, dst );
    }
}


/**********************************************************************************************/
OcclusionSegmentation::OcclusionSegmentation(const Params &params) {

    // init
    this->params = params;
}

OcclusionSegmentation::~OcclusionSegmentation(){
}

/*!
 * \brief OcclusionSegmentation::compute This is the main method to be called from outside. It is the full detection pipeline.
 */
void OcclusionSegmentation::compute(const cv::Mat im0, const cv::Mat im1,
                                    const cv::Mat flow_f, const cv::Mat flow_b){

    // Sanitizing inputs
    if ( im0.type()==CV_8UC1  ) {
        cv::cvtColor( im0, this->i_0, cv::COLOR_GRAY2BGR );
    }else{
        this->i_0 = im0.clone();
    }

    if ( im1.type()==CV_8UC1  ) {
        cv::cvtColor( im1, this->i_1, cv::COLOR_GRAY2BGR );
    }else{
        this->i_1 = im1.clone();
    }

    // Computing useful stuff
    this->i_0.convertTo(this->h_0, CV_32F);
    this->i_1.convertTo(this->h_1, CV_32F);

    cv::Mat gr_0, gr_1;
    cv::cvtColor(this->i_0, gr_0, cv::COLOR_BGR2GRAY);
    cv::cvtColor(this->i_1, gr_1, cv::COLOR_BGR2GRAY);

    if ( flow_f.empty() ) {
        this->flow_f = this->computeFlow(gr_0, gr_1);
    }

    if ( flow_b.empty() ) {
        this->flow_b = this->computeFlow(gr_1, gr_0);
    }

    cv::Laplacian(gr_0, this->gra, CV_64F);
    cv::normalize(this->gra, this->gra, 0, 1.0, cv::NORM_MINMAX, CV_64FC1);

    this->gmm_likelihoodmap = this->computeGmmLikelihoodMap();
    this->flo_differencemap = this->computeFloDifferenceMap();
    this->col_differencemap = this->computeColDifferenceMap();

    // MRF
    this->occlusion_map = this->solveMRF();
}

cv::Mat OcclusionSegmentation::solveMRF(){
    const int num_occ_labels = 2;
    GCoptimizationGeneralGraph graph( this->h_0.rows * this->h_0.cols, num_occ_labels );

    // For each point p=(x,y) and label, compose E
    int p = 0;
    for (int y=0; y<this->h_0.rows; ++y) {
        for (int x=0; x<this->h_0.cols; ++x) {

            cv::Vec3f i_0_p = this->h_0.at<cv::Vec3f>(y,x);
            cv::Vec2f f_p = this->flow_f.at<cv::Vec2f>(y,x);
            cv::Point2f p_0_pf = cv::Point2f( float(x)+f_p[0], float(y)+f_p[1] );

            double data_cost;

            if ( p_0_pf.x<0 || p_0_pf.x>=i_0.cols || p_0_pf.y<0 || p_0_pf.y>=i_0.rows )
                data_cost = 1000.0; //Just set an arbitrary high cost when the flow vectors fall outside the image
            else
                data_cost = this->params.beta_flow * this->flo_differencemap.at<double>(y,x) +
                            this->params.beta_gmm  * this->gmm_likelihoodmap.at<double>(y,x) +
                            this->params.beta_rgb  * this->col_differencemap.at<double>(y,x);

            //std::cout<<data_cost<<std::endl;

            graph.setDataCost( p, 0, data_cost ); //VISIBLE
            graph.setDataCost( p, 1, this->params.epsilon ); //NOT-VISIBLE
            ++p;
        }
    }

    // Set neighborhood and weights (image dependent)
    {
    int q = 0;
    int p = 0;
    for (int y=0; y<i_0.rows; ++y) {
        for (int x=0; x<i_0.cols; ++x) {
            // Neighboors
            double weight_neighboors;
            if ( x>0 ) { //left
                q = p-1;
                weight_neighboors = this->params.alpha_rgb * cv::norm( this->h_0.at<cv::Vec3f>(y,x) - this->h_0.at<cv::Vec3f>(y,x-1) );
                weight_neighboors += this->params.alpha_gra * cv::norm( this->gra.at<double>(y,x) - this->gra.at<double>(y, x-1) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            if ( x>0 && y>0 ) { //upleft
                q = p-1-i_0.cols;
                weight_neighboors = this->params.alpha_rgb * cv::norm( this->h_0.at<cv::Vec3f>(y,x) - this->h_0.at<cv::Vec3f>(y-1,x-1) );
                weight_neighboors += this->params.alpha_gra * cv::norm( this->gra.at<double>(y,x) - this->gra.at<double>(y-1, x-1) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            if ( y>0 ) { //up
                q = p-i_0.cols;
                weight_neighboors = this->params.alpha_rgb * cv::norm( this->h_0.at<cv::Vec3f>(y,x) - this->h_0.at<cv::Vec3f>(y-1,x) );
                weight_neighboors += this->params.alpha_gra * cv::norm( this->gra.at<double>(y,x) - this->gra.at<double>(y-1, x) );
                graph.setNeighbors(p,q,weight_neighboors);            }

            if ( x<i_0.cols-1 && y>0 ) { //upright
                q = p-i_0.cols+1;
                weight_neighboors = this->params.alpha_rgb * cv::norm( this->h_0.at<cv::Vec3f>(y,x) - this->h_0.at<cv::Vec3f>(y-1,x+1) );
                weight_neighboors += this->params.alpha_gra * cv::norm( this->gra.at<double>(y,x) - this->gra.at<double>(y-1, x+1) );
                graph.setNeighbors(p,q,weight_neighboors);
                graph.setNeighbors(p,q,weight_neighboors);
            }

            p++;
        }
    }
    }

    // Build Label cost matrix (and no label costs) ==> POTTS MODEL
    for ( int label1=0; label1<num_occ_labels; ++label1 ) {
        for ( int label2=0; label2<num_occ_labels; ++label2 ) {
            if (label1!=label2) {
                graph.setSmoothCost(label1,label2,this->params.potts);
            }else {
                graph.setSmoothCost(label1,label2,0.0);
            }
        }
    }

    // Initialize with previously computed occlusions
    cv::Mat occlusions;
    if ( !this->occlusion_map.empty() ) {
        occlusions = this->occlusion_map.clone();
        int p = 0;
        for (int y=0; y<i_0.rows; ++y) {
            for (int x=0; x<i_0.cols; ++x) {
                graph.setLabel( p, this->occlusion_map.at<uchar>(y,x) );
            }
        }
    }else{
        occlusions = cv::Mat::zeros(this->i_0.rows, this->i_1.cols, CV_8UC1);
    }

    graph.setVerbosity(1);

    //Minimize with expansions
    graph.expansion(1);

    //Get labels after minimization of the energy
    {
        int p = 0;
        for (int y=0; y<i_0.rows; ++y) {
            for (int x=0; x<i_0.cols; ++x) {
                occlusions.at<uchar>(y,x) = 255*(uchar)graph.whatLabel(p);
                ++p;
            }
        }
    }

    return occlusions.clone();
}

cv::Mat OcclusionSegmentation::computeFlow(const cv::Mat gr0, const cv::Mat gr1){
    cv::Mat flow;

    cv::Ptr<DenseOpticalFlow> deepflow = cv::optflow::createOptFlow_DeepFlow();
    deepflow->calc(gr0, gr1, flow);

    return flow;
}

cv::Mat OcclusionSegmentation::computeGmmLikelihoodMap(){

    Slic slicer(700,10);
    std::map<int, std::vector< cv::Point > > superpixels;
    cv::Mat superpixel_mat;
    cv::Mat filteredim;
    _bilateralFilter(this->i_0, filteredim, 3, 0.1, 0.1);
    cv::Mat filteredcrossim;
    _xbilateralFilter(this->i_0, this->i_1, this->flow_f, filteredcrossim, 3, 0.1, 0.1);

    // TRAIN A GMM FOR EVERY SUPERPIXEL //
    slicer.findSLICSuperPixels( this->i_0, superpixels, superpixel_mat );
    std::vector<GMM> local_models;
    trainGmmForPixelsets( this->i_0, superpixels, local_models );
    cv::Mat lmap = evalImage(filteredcrossim, superpixels, local_models);
    clipMat(this->flow_f, lmap, 0, 255);
    cv::Mat norml;
    cv::normalize(lmap, norml, 0, 1.0, cv::NORM_MINMAX, CV_64FC1);

    return norml;
}

cv::Mat OcclusionSegmentation::computeFloDifferenceMap(){
    cv::Mat flowdiff( this->i_0.size(), CV_64FC1, cv::Scalar(0.0) );
    for ( int y=0; y<this->flow_f.rows; ++y ) {
        for ( int x=0; x<this->flow_f.cols; ++x ) {
            cv::Vec2f ffv = this->flow_f.at<cv::Vec2f>(y,x);
            cv::Vec2f bfv = getSubpixFlow(this->flow_b, cv::Point2f( float(x)+ffv[0], float(y)+ffv[1] ));
            double normf = cv::norm(ffv+bfv, cv::NORM_L2);
            flowdiff.at<double>(y,x) = normf;
        }
    }
    cv::Mat normf;
    cv::normalize(flowdiff, normf, 0, 1.0, cv::NORM_MINMAX, CV_64FC1);
    return normf;
}

cv::Mat OcclusionSegmentation::computeColDifferenceMap(){
    cv::Mat colordiff( this->h_0.size(), CV_64FC1, cv::Scalar(0.0) );
    for ( int y=0; y<this->h_0.rows; ++y ) {
        for ( int x=0; x<this->h_0.cols; ++x ) {
            cv::Vec3f cola = this->h_0.at<cv::Vec3f>(y,x);
            cv::Vec2f ffv = this->flow_f.at<cv::Vec2f>(y,x);
            cv::Vec3f colb = getSubpixColor3f(this->h_1, cv::Point2f( float(x)+ffv[0], float(y)+ffv[1] ));
            double normc = cv::norm(cola-colb, cv::NORM_L2);
            colordiff.at<double>(y,x) = normc;
        }
    }
    //clipMat(this->flow_f, colordiff);
    cv::Mat normc;
    cv::normalize(colordiff, normc, 0, 1.0, cv::NORM_MINMAX, CV_64FC1);
    return normc;
}

cv::Mat OcclusionSegmentation::getOcclusions() const {
    return this->occlusion_map.clone();
}

void BilateralFilter::filtWithFlow(const Mat &imIn, const Mat &flow, Mat &imOut, float sigmaFlow){

    imOut = cv::Mat::zeros(imIn.size(), imIn.type());

    int d = m_radius*2 + 1;

    if (imIn.type()==CV_8UC3)
    for (int y=0; y<imIn.rows; ++y) {

        uchar *rowPtr = (uchar *)imIn.ptr(y);
        uchar *outRowPtr = (uchar *)imOut.ptr(y);

        for (int x=0; x<imIn.cols; ++x, rowPtr+=3, outRowPtr+=3) {

            const Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//imIn.at<Vec3b>(y,x);
            Vec3f sumfil = Vec3f(.0f,.0f,.0f);
            float sumnor = 0.0f;
            Vec2f flow_p = flow.at<Vec2f>(y,x);

            // The region of interest
            for (int resy=y-m_radius; resy<y+m_radius; ++resy) {
                for (int resx=x-m_radius; resx<x+m_radius; ++resx) {

                    int useresy = borderInterpolate(resy, imIn.rows, cv::BORDER_REPLICATE);
                    int useresx = borderInterpolate(resx, imIn.cols, cv::BORDER_REPLICATE);

                    Vec3b itval = imIn.at<Vec3b>(useresy,useresx);
                    Vec2f itflo = flow.at<Vec2f>(useresy,useresx);

                    float colnor = cv::norm(imval-itval,cv::NORM_L2SQR);
                    float spanor = ((x-resx)*(x-resx)+(y-resy)*(y-resy));
                    float flonor = cv::norm(flow_p-itflo,cv::NORM_L2SQR);

                    float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) - flonor/(2*sigmaFlow) );

                    sumnor += weight;
                    sumfil[0] += float(itval[0])*weight;
                    sumfil[1] += float(itval[1])*weight;
                    sumfil[2] += float(itval[2])*weight;
                }
            }
            outRowPtr[0] = sumfil[0] / (sumnor);
            outRowPtr[1] = sumfil[1] / (sumnor);
            outRowPtr[2] = sumfil[2] / (sumnor);
        }
    }

}

void BilateralFilter::filt(const Mat &imIn, Mat &imOut) {
    //CV_Assert( imIn.type()==CV_8UC1 );
    imOut = cv::Mat::zeros(imIn.size(), imIn.type());

    int d = m_radius*2 + 1;


    if (imIn.type()==CV_8UC1)
    for (int y=m_radius; y<imIn.rows-m_radius; ++y) {
        for (int x=m_radius; x<imIn.cols-m_radius; ++x) {

            float imval = imIn.at<uchar>(y,x);
            float sumfil = 0;
            float sumnor = 0;

            // The region of interest
            for (int resy=y-m_radius; resy<y+m_radius; ++resy) {
                for (int resx=x-m_radius; resx<x+m_radius; ++resx) {

                    int useresy = borderInterpolate(resy, imIn.rows, cv::BORDER_REPLICATE);
                    int useresx = borderInterpolate(resx, imIn.cols, cv::BORDER_REPLICATE);

                    float itval = imIn.at<uchar>(useresy,useresx);
                    float weight = std::exp( -((x-resx)*(x-resx)+(y-resy)*(y-resy))/(2*m_sigma_space) - (imval-itval)*(imval-itval)/(2*m_sigma_color) );

                    sumnor += weight;
                    sumfil += itval*weight;
                }
            }
            float filtered_val = sumfil / (sumnor+std::numeric_limits<float>::epsilon());
            imOut.at<uchar>(y,x)=(uchar)filtered_val;
        }
    }

    if (imIn.type()==CV_8UC3)
    for (int y=0; y<imIn.rows; ++y) {

        uchar *rowPtr = (uchar *)imIn.ptr(y);
        uchar *outRowPtr = (uchar *)imOut.ptr(y);

        for (int x=0; x<imIn.cols; ++x, rowPtr+=3, outRowPtr+=3) {

            const Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//imIn.at<Vec3b>(y,x);
            Vec3f sumfil = Vec3f(.0f,.0f,.0f);
            float sumnor = 0.0f;

            // The region of interest
            for (int resy=y-m_radius; resy<y+m_radius; ++resy) {
                for (int resx=x-m_radius; resx<x+m_radius; ++resx) {

                    int useresy = borderInterpolate(resy, imIn.rows, cv::BORDER_REPLICATE);
                    int useresx = borderInterpolate(resx, imIn.cols, cv::BORDER_REPLICATE);

                    Vec3b itval = imIn.at<Vec3b>(useresy,useresx);

                    float colnor = cv::norm(imval-itval,cv::NORM_L2SQR);
                    float spanor = ((x-resx)*(x-resx)+(y-resy)*(y-resy));

                    float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) );

                    sumnor += weight;
                    sumfil[0] += float(itval[0])*weight;
                    sumfil[1] += float(itval[1])*weight;
                    sumfil[2] += float(itval[2])*weight;
                }
            }

            outRowPtr[0] = sumfil[0] / (sumnor);
            outRowPtr[1] = sumfil[1] / (sumnor);
            outRowPtr[2] = sumfil[2] / (sumnor);
        }
    }
}


void XBilateralFilter::filtWithFlow(const Mat &im1, const Mat &im2, const Mat &flow, Mat &imOut, float sigmaFlow) {

    /*cv::imshow("i1",im1);
    cv::imshow("i2",im2);
    cv::Mat fc1, fc2;
    getColorMat(flow, fc1);
    getColorMat(flow2, fc2);
    cv::imshow("fc1",fc1);
    cv::imshow("fc2",fc2);
    cv::waitKey(0);*/

    imOut = cv::Mat::zeros(im1.size(), im1.type());

    if (im1.type()==CV_8UC3)
        for (int y=0; y<im1.rows; ++y) {

            uchar *rowPtr = (uchar *)im1.ptr(y);
            uchar *outRowPtr = (uchar *)imOut.ptr(y);

            for (int x=0; x<im1.cols; ++x, rowPtr+=3, outRowPtr+=3) {

                Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//im1.at<Vec3b>(y,x);
                Vec3f sumfil = Vec3f(.0f,.0f,.0f);
                float sumnor = 0.0f;

                // The region of interest in the image 2
                Vec2f f = flow.at<Vec2f>(y,x);
                float ry = y + f[1];
                float rx = x + f[0];
                for (int iy=-m_radius; iy<+m_radius; ++iy) {
                    for (int ix=-m_radius; ix<+m_radius; ++ix) {

                        Vec3b itval = getSubpixColor(im2, Point2f(ix+rx,iy+ry));

                        Vec3b itimval = getSubpixColor(im1, Point2f(ix+x,iy+y));
                        Vec2f itflval = getSubpixFlow(flow, Point2f(ix+x,iy+y));

                        float colnor = cv::norm(itimval-imval,cv::NORM_L2SQR);
                        float flonor = cv::norm(itflval-f,cv::NORM_L2SQR);
                        float spanor = ix*ix+iy*iy;

                        float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) - flonor/(2*sigmaFlow));

                        sumnor += weight;
                        sumfil[0] += float(itval[0])*weight;
                        sumfil[1] += float(itval[1])*weight;
                        sumfil[2] += float(itval[2])*weight;
                    }
                }
                outRowPtr[0] = sumfil[0] / (sumnor);
                outRowPtr[1] = sumfil[1] / (sumnor);
                outRowPtr[2] = sumfil[2] / (sumnor);
            }
        }

    //cv::imshow("io",imOut);
    //cv::waitKey(0);

}

void XBilateralFilter::filt(const Mat &im1, const Mat &im2, const Mat &flow, Mat &imOut) {
    //CV_Assert( imIn.type()==CV_8UC1 );
    imOut = cv::Mat::zeros(im1.size(), im1.type());

    if (im1.type()==CV_8UC3)
    for (int y=0; y<im1.rows; ++y) {

        uchar *rowPtr = (uchar *)im1.ptr(y);
        uchar *outRowPtr = (uchar *)imOut.ptr(y);

        for (int x=0; x<im1.cols; ++x, rowPtr+=3, outRowPtr+=3) {

            Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//im1.at<Vec3b>(y,x);
            Vec3f sumfil = Vec3f(.0f,.0f,.0f);
            float sumnor = 0.0f;

            // The region of interest in the image 2
            Vec2f f = flow.at<Vec2f>(y,x);
            float ry = y + f[1];
            float rx = x + f[0];
            for (int iy=-m_radius; iy<+m_radius; ++iy) {
                for (int ix=-m_radius; ix<+m_radius; ++ix) {

                    Point2f itp(ix+rx,iy+ry);
                    Vec3b itval = getSubpixColor(im2, itp);

                    float colnor = cv::norm(imval-itval,cv::NORM_L2SQR);
                    float spanor = ix*ix+iy*iy;

                    float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) );

                    sumnor += weight;
                    sumfil[0] += float(itval[0])*weight;
                    sumfil[1] += float(itval[1])*weight;
                    sumfil[2] += float(itval[2])*weight;
                }
            }
            outRowPtr[0] = sumfil[0] / (sumnor);
            outRowPtr[1] = sumfil[1] / (sumnor);
            outRowPtr[2] = sumfil[2] / (sumnor);
        }
    }
}

// this is the one that uses all the ideas: structure rpeservation and so on...
void XBilateralFilter::filt2(const Mat &im1, const Mat &im2, const Mat &flow, Mat &imOut) {
    //CV_Assert( imIn.type()==CV_8UC1 );
    imOut = cv::Mat::zeros(im1.size(), im1.type());

    if (im1.type()==CV_8UC3)
    for (int y=0; y<im1.rows; ++y) {

        uchar *rowPtr = (uchar *)im1.ptr(y);
        uchar *outRowPtr = (uchar *)imOut.ptr(y);

        for (int x=0; x<im1.cols; ++x, rowPtr+=3, outRowPtr+=3) {

            Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//im1.at<Vec3b>(y,x);
            Vec3f sumfil = Vec3f(.0f,.0f,.0f);
            float sumnor = 0.0f;

            // The region of interest in the image 2
            Point2f f = flow.at<Point2f>(y,x);
            float ry = y + f.y;
            float rx = x + f.x;
            for (int iy=-m_radius; iy<+m_radius; ++iy) {
                for (int ix=-m_radius; ix<+m_radius; ++ix) {

                    Vec3b itval = getSubpixColor(im2, Point2f(ix+rx,iy+ry)); //uses flow
                    Vec3b itimval = getSubpixColor(im1, Point2f(ix+x,iy+y)); //im1

                    float colnor = cv::norm(itimval-imval,cv::NORM_L2SQR);
                    float spanor = ix*ix+iy*iy;

                    float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) );

                    sumnor += weight;
                    sumfil[0] += float(itval[0])*weight;
                    sumfil[1] += float(itval[1])*weight;
                    sumfil[2] += float(itval[2])*weight;
                }
            }
            outRowPtr[0] = sumfil[0] / (sumnor);
            outRowPtr[1] = sumfil[1] / (sumnor);
            outRowPtr[2] = sumfil[2] / (sumnor);
        }
    }
}


void XBilateralFilter::filt3(const Mat &im1, const Mat &im2, const Mat &flow, Mat &imOut) {
    //CV_Assert( imIn.type()==CV_8UC1 );
    imOut = cv::Mat::zeros(im1.size(), im1.type());

    if (im1.type()==CV_8UC3)
    for (int y=0; y<im1.rows; ++y) {

        uchar *rowPtr = (uchar *)im1.ptr(y);
        uchar *outRowPtr = (uchar *)imOut.ptr(y);

        for (int x=0; x<im1.cols; ++x, rowPtr+=3, outRowPtr+=3) {

            Vec3b imval = Vec3b(rowPtr[0], rowPtr[1], rowPtr[2]);//im1.at<Vec3b>(y,x);
            Vec3f sumfil = Vec3f(.0f,.0f,.0f);
            float sumnor = 0.0f;

            // The region of interest in the image 1
            for (int iy=y-m_radius; iy<y+m_radius; ++iy) {
                for (int ix=x-m_radius; ix<x+m_radius; ++ix) {

                    int useresy = cv::borderInterpolate(iy, flow.rows, cv::BORDER_REPLICATE);
                    int useresx = cv::borderInterpolate(ix, flow.cols, cv::BORDER_REPLICATE);

                    Point2f f = flow.at<Point2f>(useresy, useresx);
                    float ry = iy + f.y;
                    float rx = ix + f.x;

                    Point2f itp(rx,ry);
                    Vec3b itval = getSubpixColor(im2, itp);

                    float colnor = cv::norm(imval-itval,cv::NORM_L2SQR);
                    float spanor = (ix-x)*(ix-x)+(iy-y)*(iy-y);

                    float weight = std::exp( -spanor/(2*m_sigma_space) - colnor/(2*m_sigma_color) );

                    sumnor += weight;
                    sumfil[0] += float(itval[0])*weight;
                    sumfil[1] += float(itval[1])*weight;
                    sumfil[2] += float(itval[2])*weight;
                }
            }
            outRowPtr[0] = sumfil[0] / (sumnor);
            outRowPtr[1] = sumfil[1] / (sumnor);
            outRowPtr[2] = sumfil[2] / (sumnor);
        }
    }
}


#include "occseg.hpp"


namespace {
    void RectToMat( cv::Rect window, cv::Mat& labelMat, cv::Size imsize, int label, const cv::Mat& applicationMask ) {
        labelMat = cv::Mat::zeros( imsize, CV_8UC1 );
        cv::rectangle(labelMat, window, cv::Scalar(label), cv::FILLED);
        if (!applicationMask.empty())
            cv::bitwise_and( 255-applicationMask, labelMat, labelMat );
    }

    cv::Vec3f getSubpixColor(const cv::Mat& img, cv::Point2f pt) {
        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   img.cols, cv::BORDER_REPLICATE);
        int x1 = cv::borderInterpolate(x+1, img.cols, cv::BORDER_REPLICATE);
        int y0 = cv::borderInterpolate(y,   img.rows, cv::BORDER_REPLICATE);
        int y1 = cv::borderInterpolate(y+1, img.rows, cv::BORDER_REPLICATE);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float b = (img.at<cv::Vec3f>(y0, x0)[0] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[0] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[0] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[0] * a) * c;
        float g = (img.at<cv::Vec3f>(y0, x0)[1] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[1] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[1] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[1] * a) * c;
        float r = (img.at<cv::Vec3f>(y0, x0)[2] * (1.f - a) + img.at<cv::Vec3f>(y0, x1)[2] * a) * (1.f - c)
                               + (img.at<cv::Vec3f>(y1, x0)[2] * (1.f - a) + img.at<cv::Vec3f>(y1, x1)[2] * a) * c;

        return cv::Vec3f(b, g, r);
    }

    cv::Vec2f getSubpixGradient(const cv::Mat& gx, const cv::Mat& gy, cv::Point2f pt) {
        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   gx.cols, cv::BORDER_REPLICATE);
        int x1 = cv::borderInterpolate(x+1, gx.cols, cv::BORDER_REPLICATE);
        int y0 = cv::borderInterpolate(y,   gx.rows, cv::BORDER_REPLICATE);
        int y1 = cv::borderInterpolate(y+1, gx.rows, cv::BORDER_REPLICATE);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float g0 = (gx.at<float>(y0, x0) * (1.f - a) + gx.at<float>(y0, x1) * a) * (1.f - c)
                               + (gx.at<float>(y1, x0) * (1.f - a) + gx.at<float>(y1, x1) * a) * c;
        float g1 = (gy.at<float>(y0, x0) * (1.f - a) + gy.at<float>(y0, x1) * a) * (1.f - c)
                               + (gy.at<float>(y1, x0) * (1.f - a) + gy.at<float>(y1, x1) * a) * c;
        return cv::Vec2f(g0, g1);
    }

    cv::Vec2f getSubpixFlow(const cv::Mat& flow, cv::Point2f pt) {
        CV_Assert(!flow.empty());
        CV_Assert(flow.channels() == 2);

        int x = (int)pt.x;
        int y = (int)pt.y;

        int x0 = cv::borderInterpolate(x,   flow.cols, cv::BORDER_REFLECT_101);
        int x1 = cv::borderInterpolate(x+1, flow.cols, cv::BORDER_REFLECT_101);
        int y0 = cv::borderInterpolate(y,   flow.rows, cv::BORDER_REFLECT_101);
        int y1 = cv::borderInterpolate(y+1, flow.rows, cv::BORDER_REFLECT_101);

        float a = pt.x - (float)x;
        float c = pt.y - (float)y;

        float u = (flow.at<cv::Vec2f>(y0, x0)[0] * (1.f - a) + flow.at<cv::Vec2f>(y0, x1)[0] * a) * (1.f - c)
                               + (flow.at<cv::Vec2f>(y1, x0)[0] * (1.f - a) + flow.at<cv::Vec2f>(y1, x1)[0] * a) * c;
        float v = (flow.at<cv::Vec2f>(y0, x0)[1] * (1.f - a) + flow.at<cv::Vec2f>(y0, x1)[1] * a) * (1.f - c)
                               + (flow.at<cv::Vec2f>(y1, x0)[1] * (1.f - a) + flow.at<cv::Vec2f>(y1, x1)[1] * a) * c;

        return cv::Vec2f(u, v);
    }

    static cv::Mat bwfwOcclusionMap( const cv::Mat& i_0, const cv::Mat& i_1, float epsilon_v ) {

        cv::Ptr< cv::DenseOpticalFlow > flow_estimator = cv::optflow::createOptFlow_DeepFlow();
        cv::Mat ff, bf, g0, g1;

        cv::cvtColor(i_0, g0, cv::COLOR_BGR2GRAY);
        cv::cvtColor(i_1, g1, cv::COLOR_BGR2GRAY);
        flow_estimator->calc( g0, g1, ff );
        flow_estimator->calc( g1, g0, bf );

        cv::Mat occ(i_0.size(),CV_8UC1);
        for ( int y=0; y<ff.rows; ++y ) {
            for ( int x=0; x<ff.cols; ++x ) {
                cv::Vec2f ffv = ff.at<cv::Vec2f>(y,x);
                cv::Vec2f bfv = getSubpixFlow(bf,cv::Point2f( float(x)+ffv[0], float(y)+ffv[1] ));
                float normf = cv::norm(ffv+bfv, cv::NORM_L2);
                occ.at<uchar>(y,x) = (normf<epsilon_v)?0:1;
            }
        }
        return occ;
    }

}

Motion2D::~Motion2D(){

}

void Motion2D::computeModelAtWindow(const cv::Mat& im1, const cv::Mat& im2, cv::Rect window , cv::Mat &modelMat, const cv::Mat &application_mask,  bool use_model_initialization) {
    cv::Mat gray1, gray2, support;

    CV_Assert( im1.depth()==im2.depth() && im1.size()==im2.size() && im1.depth()==CV_8U );

    if ( im1.type()==CV_8UC3  ) {
        cv::cvtColor( im1, gray1, cv::COLOR_BGR2GRAY );
    }else{
        gray1 = im1;
    }

    if ( im2.type()==CV_8UC3  ){
        cv::cvtColor( im2, gray2, cv::COLOR_BGR2GRAY );
    }else{
        gray2 = im2;
    }

    //gray1.convertTo(gray1, CV_16SC1);
    //gray2.convertTo(gray2, CV_16SC1);

    // Prepare Support
    RectToMat(window, support, gray1.size(), m_label, application_mask);

    // Allocating and building the image pyramids
    CMotion2DPyramid pyramid1;	// Pyramid on image 1
    CMotion2DPyramid pyramid2;	// Pyramid on image 2

    //pyramid1.allocate(gray1.rows, gray1.cols, m_pyr_nlevels);
    //pyramid2.allocate(gray2.rows, gray2.cols, m_pyr_nlevels);
    pyramid1.build( gray1.ptr(), gray1.rows, gray1.cols, m_pyr_nlevels );
    pyramid2.build( gray2.ptr(), gray2.rows, gray2.cols, m_pyr_nlevels );
    //pyramid1.build( gray1.ptr() );
    //pyramid2.build( gray2.ptr() );


    // Motion Model
    Motion2D::Model model;
    model.reset();                  // Set all the parameters to zero
    model.setIdModel(m_model_id);   // Set the model id
    model.setVarLight(m_var_light); // Set the illumination variation estimation
    model.setOrigin(0, 0);          // Origin

    double parameters[CMotion2DModel::MDL_NMAX_COEF];

    if (!use_model_initialization){
        modelMat.create(2, 6, CV_64F);
    }else{
        CV_Assert( !modelMat.empty() );
        parameters[2]  = modelMat.at<double>(0,0);
        parameters[3]  = modelMat.at<double>(0,1);
        parameters[4]  = modelMat.at<double>(1,0);
        parameters[5]  = modelMat.at<double>(1,1);
        parameters[0]  = modelMat.at<double>(0,2);
        parameters[1]  = modelMat.at<double>(1,2);
        parameters[6]  = modelMat.at<double>(0,3);
        parameters[7]  = modelMat.at<double>(0,4);
        parameters[8]  = modelMat.at<double>(0,5);
        parameters[9]  = modelMat.at<double>(1,3);
        parameters[10] = modelMat.at<double>(1,4);
        parameters[11] = modelMat.at<double>(1,5);
        model.setParameters( parameters );
    }

    // Estimate Motion
    m_estimator.estimate(pyramid1, pyramid2, support.ptr(), m_label, model, use_model_initialization);
    m_motionModel = model;


    model.getParameters(parameters);
    modelMat.at<double>(0,0) = parameters[2];
    modelMat.at<double>(0,1) = parameters[3];
    modelMat.at<double>(1,0) = parameters[4];
    modelMat.at<double>(1,1) = parameters[5];
    modelMat.at<double>(0,2) = parameters[0];
    modelMat.at<double>(1,2) = parameters[1];
    modelMat.at<double>(0,3) = parameters[6];
    modelMat.at<double>(0,4) = parameters[7];
    modelMat.at<double>(0,5) = parameters[8];
    modelMat.at<double>(1,3) = parameters[9];
    modelMat.at<double>(1,4) = parameters[10];
    modelMat.at<double>(1,5) = parameters[11];

    // Release and return
    pyramid1.destroy();
    pyramid2.destroy();
}

void Motion2D::computeFlowField( cv::Mat &flow, cv::Size flowsize) {

    flow = cv::Mat::zeros(flowsize, CV_32FC2);

    double parameters[CMotion2DModel::MDL_NMAX_COEF];
    m_motionModel.getParameters(parameters);

    for (int x=0; x<flow.cols; ++x) {
        for (int y=0; y<flow.rows; ++y) {
            flow.at<cv::Vec2f>(y,x) = cv::Vec2f( parameters[2]*double(x)+parameters[3]*double(y) + parameters[0] + parameters[6]*double(x*x)+parameters[7]*double(x*y)+parameters[8]*double(y*y),
                                                 parameters[4]*double(x)+parameters[5]*double(y) + parameters[1] + parameters[9]*double(x*x)+parameters[10]*double(x*y)+parameters[11]*double(y*y) );
        }
    }
}


/**************************************************************************************/
/*!
 * \brief MotionCandidatesGenerator::generate2 this window generates equal side size windows
 * \param windows
 */
void MotionCandidatesGenerator::generate2(std::vector<std::vector<cv::Rect> > &windows) {
    windows.clear();
    int initial_size = MIN(m_baseSize.width, m_baseSize.height );
    cv::Rect full_window( 0, 0, m_baseSize.width, m_baseSize.height );
    cv::Rect curr_window( 0, 0, initial_size, initial_size );

    std::vector< cv::Rect > initial_level;
    initial_level.push_back( full_window );
    windows.push_back( initial_level );

    for ( int level=0; level<m_numLevels; ++level ) {
        std::vector< cv::Rect > levelWins;
        curr_window.width  /= level + 1;
        curr_window.height /= level + 1;

        for ( curr_window.x = 0; curr_window.x<full_window.width;  curr_window.x += curr_window.width /3 ) {
        for ( curr_window.y = 0; curr_window.y<full_window.height; curr_window.y += curr_window.height/3 ) {

            if ( (curr_window & full_window).area()>1000 )
                levelWins.push_back( (curr_window & full_window) );
        }
        }
        windows.push_back(levelWins);
    }
}

/*!
 * \brief MotionCandidatesGenerator::generate creates windows based on the Bouthemy's process
 * \param windows
 */
void MotionCandidatesGenerator::generate(std::vector<std::vector<cv::Rect> > &windows) {

    windows.clear();
    cv::Rect baseRect( 0, 0, m_baseSize.width, m_baseSize.height );
    cv::Size currSize = m_baseSize;
    for ( int level=0; level<m_numLevels; ++level ) {
        cv::Point inip(  -currSize.width/2.0,  -currSize.height/2.0 );
        cv::Point endp( m_baseSize.width + inip.x, m_baseSize.height + inip.y );
        std::vector< cv::Rect > levelWins;

        cv::Point it;
       for ( it.x=inip.x; it.x<=endp.x; it.x+=currSize.width/2.0 ) {
            for ( it.y=inip.y; it.y<=endp.y; it.y+=currSize.height/2.0 ) {
                cv::Rect currRect(it, currSize);
                currRect &= baseRect;
                levelWins.push_back( currRect );
                /*std::cout<<"*************"<<std::endl;
                std::cout<<inip<<std::endl;
                std::cout<<it<<std::endl;
                std::cout<<currRect<<std::endl;*/
            }
        }
        windows.push_back(levelWins);
        currSize.width  /= 2;
        currSize.height /= 2;
    }
}

static cv::Mat fromPointAffineToMotionAffine( const cv::Mat& affine, const std::vector<cv::Point> &pts1, const std::vector<cv::Point> &pts2 ) {
    // the output
    cv::Mat output(2, 6, CV_64F, cv::Scalar(0.0)); //general format size

    // the input is an affine transformation matrix between sets of points
    // but I want the affine model of the motion!

    // We are doing this transformation by solving an A*x=b problem

    // Declare and fill A
    cv::Mat A(2*pts1.size(),6,CV_64FC1);
    int pind=0;
    for (int row=0; row<A.rows; ++row) {
        if (row%2==0) {
            cv::Mat evenrow = (cv::Mat_<double>(1,6) << 1.0, 0.0, pts1[pind].x, pts1[pind].y, 0.0, 0.0);
            evenrow.copyTo( A.row(row) );
        }else{
            cv::Mat oddrow = (cv::Mat_<double>(1,6) << 0.0, 1.0, 0.0, 0.0, pts1[pind].x, pts1[pind].y);
            oddrow.copyTo( A.row(row) );
            ++pind; // this only increments every two rows
        }
    }

    cv::Mat B(2*pts1.size(),1,CV_64FC1);
    pind=0;
    for (int row=0; row<B.rows; ++row) {
        if (row%2==0) {
            B.at<double>(row,0) = double(pts2[pind].x-pts1[pind].x);
        }else{
            B.at<double>(row,0) = double(pts2[pind].y-pts1[pind].y);
            ++pind;
        }
    }

    cv::Mat X;
    cv::solve(A,B,X,cv::DECOMP_SVD);
    //std::cout<<X<<std::endl;

    output.at<double>(0,0) = X.at<double>(2);
    output.at<double>(0,1) = X.at<double>(3);
    output.at<double>(0,2) = X.at<double>(0);
    output.at<double>(1,0) = X.at<double>(4);
    output.at<double>(1,1) = X.at<double>(5);
    output.at<double>(1,2) = X.at<double>(1);

    return output;
}

static inline
void matchesInWindow( const std::vector<cv::Point> &pts1, const std::vector<cv::Point> &pts2, std::vector<cv::Point> &opts1, std::vector<cv::Point> &opts2, cv::Rect win ){

    opts1.clear();
    opts2.clear();

    for ( int pind = 0; pind<pts1.size(); ++pind ) {
        cv::Point p = pts1[pind];
        // the point is inside the window
        if ( p.x>=win.x && p.y>=win.y && (p.x<win.x+win.width) && (p.y<win.y+win.height) ) {
            opts1.push_back( p );
            opts2.push_back( pts2[pind] );
        }
    }
}

void MotionCandidatesGenerator::buildParametricMotionWindowsStaringFromMatches(
        const cv::Mat &im1, const cv::Mat &im2,
        std::vector<ParamWindow> &motionCandidates, const cv::Mat &applicationMask,
        const std::vector<cv::Point> &pts1, const std::vector<cv::Point> &pts2
        ){

    cv::Size flowsize = im1.size();
    motionCandidates.clear();
    std::vector< std::vector<cv::Rect> > windows;
    this->generate2(windows);

    for ( int level=0; level<m_numLevels+1; ++level ) {
        std::vector<cv::Rect>& levelWins = windows[level];

        for ( int win=0; win<levelWins.size(); ++win ) {

            cv::Rect r =  levelWins[win];
            std::vector<cv::Point> pts1_in_r, pts2_in_r;
            matchesInWindow( pts1, pts2, pts1_in_r, pts2_in_r, r );

            /*
            cv::Mat output_buffer = im1/2 + im2/2;
            for ( int pi = 0; pi<pts1_in_r.size(); ++pi ) {
              cv::line( output_buffer, pts1_in_r[pi], pts2_in_r[pi], cv::Scalar(255,255,0), 1 );
              cv::circle(output_buffer, pts1_in_r[pi], 2, cv::Scalar(255,0,0), 2 );
              cv::circle(output_buffer, pts2_in_r[pi], 2, cv::Scalar(0,255,0), 2 );
            }

            cv::imshow("matches", output_buffer); cv::waitKey(0);
            */

            cv::Mat model;

            if ( pts1_in_r.size() > 6 ){
                model = fromPointAffineToMotionAffine( cv::estimateRigidTransform( pts1_in_r, pts2_in_r, true ), pts1_in_r, pts2_in_r );
                m_motionEngine.computeModelAtWindow( im1, im2, r, model, applicationMask, true );
            }else{
                m_motionEngine.computeModelAtWindow( im1, im2, r, model, applicationMask, false );
            }

            ParamWindow pw(r,model);
            pw.computeFlowField(flowsize);
            motionCandidates.push_back(pw);
        }
    }
}

void MotionCandidatesGenerator::buildParametricMotionWindows( const cv::Mat& im1, const cv::Mat& im2, std::vector< ParamWindow >& motionCandidates, const cv::Mat& applicationMask ) {

    cv::Size flowsize = im1.size();
    motionCandidates.clear();
    std::vector< std::vector<cv::Rect> > windows;
    this->generate(windows);

    for ( int level=0; level<m_numLevels; ++level ) {
        std::vector<cv::Rect>& levelWins = windows[level];

        for ( int win=0; win<levelWins.size(); ++win ) {

            cv::Rect r =  levelWins[win];
            cv::Mat model;
            m_motionEngine.computeModelAtWindow( im1, im2, r, model, applicationMask );

            ParamWindow pw(r,model);
            pw.computeFlowField(flowsize);
            motionCandidates.push_back(pw);
        }
    }
}

void MotionCandidatesGenerator::buildMotionCandidates(const cv::Mat &im1, const cv::Mat &im2, std::vector<cv::Mat> &motionCandidates, const cv::Mat& applicationMask) {

    motionCandidates.clear();
    cv::Size flowsize = im1.size();

    std::vector< std::vector<cv::Rect> > windows;
    this->generate(windows);

    for ( int level=0; level<m_numLevels; ++level ) {
        std::vector<cv::Rect>& levelWins = windows[level];

        cv::Mat f1(flowsize, CV_32FC2), f2(flowsize, CV_32FC2), f3(flowsize, CV_32FC2), f4(flowsize, CV_32FC2),
                m1(flowsize, CV_8UC1, cv::Scalar(0)), m2(flowsize, CV_8UC1, cv::Scalar(0)),
                m3(flowsize, CV_8UC1, cv::Scalar(0)), m4(flowsize, CV_8UC1, cv::Scalar(0));

        for ( int win=0; win<levelWins.size(); ++win ) {

            cv::Rect r =  levelWins[win];
            cv::Mat model, flow;
            m_motionEngine.computeModelAtWindow( im1, im2, r, model, applicationMask );
            m_motionEngine.computeFlowField( flow, flowsize );

            /* copy the flow in the rectangle r to the buffer mat */
            cv::Point endp(r.x+r.width, r.y+r.height);
            for (int y=r.y; y<endp.y; ++y) {
                for (int x=r.x; x<endp.x; ++x) {
                    if ( m1.at<uchar>(y,x)==0 ) {
                        f1.at<cv::Vec2f>(y,x) = flow.at<cv::Vec2f>(y,x);
                        m1.at<uchar>(y,x)=255;
                    }
                    else if ( m2.at<uchar>(y,x)==0 ) {
                        f2.at<cv::Vec2f>(y,x) = flow.at<cv::Vec2f>(y,x);
                        m2.at<uchar>(y,x)=255;
                    }
                    else if ( m3.at<uchar>(y,x)==0 ) {
                        f3.at<cv::Vec2f>(y,x) = flow.at<cv::Vec2f>(y,x);
                        m3.at<uchar>(y,x)=255;
                    }
                    else if ( m4.at<uchar>(y,x)==0 ) {
                        f4.at<cv::Vec2f>(y,x) = flow.at<cv::Vec2f>(y,x);
                        m4.at<uchar>(y,x)=255;
                    }
                }
            }
        } // for wins

        cv::Mat splitted1[2], splitted2[2], splitted3[3], splitted4[4];
        cv::split(f1, splitted1);
        cv::split(f2, splitted2);
        cv::split(f3, splitted3);
        cv::split(f4, splitted4);

        cv::medianBlur(splitted1[0], splitted1[0], 5);
        cv::medianBlur(splitted1[1], splitted1[1], 5);
        cv::medianBlur(splitted2[0], splitted2[0], 5);
        cv::medianBlur(splitted2[1], splitted2[1], 5);
        cv::medianBlur(splitted3[0], splitted3[0], 5);
        cv::medianBlur(splitted3[1], splitted3[1], 5);
        cv::medianBlur(splitted4[0], splitted4[0], 5);
        cv::medianBlur(splitted4[1], splitted4[1], 5);

        cv::merge(splitted1, 2, f1);
        cv::merge(splitted2, 2, f2);
        cv::merge(splitted3, 2, f3);
        cv::merge(splitted4, 2, f4);

        motionCandidates.push_back( f1 );
        motionCandidates.push_back( f2 );
        motionCandidates.push_back( f3 );
        motionCandidates.push_back( f4 );
    }// for levels
}

/**********************************************************************************************/
OcclusionSegmentation::OcclusionSegmentation(const cv::Mat& _i_0, const cv::Mat& _i_1 , int numWindowLevels_, bool use_deep_matches) {

    // Initialize everything
    m_mu_robust_penalty = 16.0f;
    m_v_student_t = 0.2f;

    if ( _i_0.type()==CV_8UC1  ) {
        cv::cvtColor( _i_0, i_0, cv::COLOR_GRAY2BGR );
    }else{
        i_0 = _i_0.clone();
    }

    if ( _i_1.type()==CV_8UC1  ){
        cv::cvtColor( _i_1, i_1, cv::COLOR_GRAY2BGR );
    }else{
        i_1 = _i_1.clone();
    }

    m_numNodes = i_0.cols * i_0.rows;
    currentFlow.create( i_0.size(), CV_32FC2 );

    i_0.convertTo( h_0, CV_32F );
    i_1.convertTo( h_1, CV_32F );

    cv::Mat gr_0, gr_1;
    cv::cvtColor(i_0,gr_0,cv::COLOR_BGR2GRAY);
    cv::cvtColor(i_1,gr_1,cv::COLOR_BGR2GRAY);
    cv::Scharr(gr_0,gx_0,CV_32F,1,0);
    cv::Scharr(gr_1,gx_1,CV_32F,1,0);
    cv::Scharr(gr_0,gy_0,CV_32F,0,1);
    cv::Scharr(gr_1,gy_1,CV_32F,0,1);

    //double minx, miny, maxx, maxy;
    //cv::minMaxIdx(gx_0, &minx, &maxx);
    //cv::minMaxIdx(gy_0, &miny, &maxy);
    //std::cout<<"G0 \\in ("<<minx<<","<<maxx<<") ^ ("<<miny<<","<<maxy<<")"<<std::endl;
    //cv::minMaxIdx(gx_1, &minx, &maxx);
    //cv::minMaxIdx(gy_1, &miny, &maxy);
    //std::cout<<"G1 \\in ("<<minx<<","<<maxx<<") ^ ("<<miny<<","<<maxy<<")"<<std::endl;

    std::cout<<" Computing deep matches "<<std::endl;
    DeepMatch matcher;
    matcher.compute( gr_0, gr_1, m_pts1, m_pts2, 2 );
    std::cout<<" Size of matches: "<<m_pts1.size()<<std::endl;

    std::cout<<" Building Parametric Motions "<<std::endl;
    occlusion_map = cv::Mat::zeros(i_0.size(),CV_8UC1);//bwfwOcclusionMap(i_0,i_1,2);
    numWindowLevels = numWindowLevels_;
    motGen = MotionCandidatesGenerator( numWindowLevels, i_0.size() );
    if (use_deep_matches)
        motGen.buildParametricMotionWindowsStaringFromMatches(i_0, i_1, motion_labels, occlusion_map,m_pts1,m_pts2);
    else
        motGen.buildParametricMotionWindows(i_0, i_1, motion_labels, occlusion_map);
    std::cout<<"The number of used windows is: "<<motion_labels.size()<<std::endl;

    m_thereIsCurrentFlow = false;
    m_thereIsCurrentFlowGT = false;

    models_indexes = cv::Mat::zeros(h_0.size(), CV_32SC1);
}

OcclusionSegmentation::~OcclusionSegmentation(){
}


static bool isPointInWindow( int x, int y, cv::Rect w ) {
    return
     ( (x >= w.x) && (x < w.x+w.width) && (y >= w.y) && (y < w.y+w.height) );
}

static double distToCenter( int x, int y, cv::Rect w ) {
    return cv::norm( cv::Vec2f(x,y) - cv::Vec2f( (w.x+w.width)/2.0, (w.y+w.height)/2.0 ) , cv::NORM_L2 );
}

/*!
 * \brief OcclusionSegmentation::compute This is the main method to be called from outside. It is the full detection pipeline.
 * \param fusedIndexes Important output, the motion models labels per pixel (It is a Mat of integers)
 * \param occlusions Important output, the occlusion map (A Mat of unsigned char)
 * \param epsilon Thresold for occludness
 * \param mu Robust parameter for data term
 * \param coef_l smoothness constant for labels
 * \param coef_o smoothness constant for occlusion
 * \param beta_l parameter to configure the importance of Image-based weightin in the label smoothness
 * \param beta_o parameter to configure the importance of Image-based weightin in the occlusion smoothness
 * \param label_cost cost per added label
 * \param num_max_iters
 */
void OcclusionSegmentation::compute(cv::Mat &fusedIndexes, cv::Mat &flowVectors, cv::Mat &occlusions,
        double epsilon, double mu, double coef_l, double coef_o, double beta_l, double beta_o, double label_cost, int num_max_iters){

    for ( int iters = 0; iters < num_max_iters; ++iters ) {
        compute_occlusionfixed(fusedIndexes, flowVectors, mu, coef_l, beta_l, label_cost);
        compute_labelsfixed(occlusions, epsilon, coef_o, beta_o);
    }
}

void OcclusionSegmentation::compute_labelsfixed(cv::Mat &occlusions, double epsilon, double coef_w, double beta_o){

    const int num_occ_labels = 2;
    GCoptimizationGeneralGraph graph( h_0.rows * h_0.cols, num_occ_labels );

    // For each point p=(x,y) and label, compose E_D(O(p))
    int p = 0;
    for (int y=0; y<i_0.rows; ++y) {
        for (int x=0; x<i_0.cols; ++x) {
            // Flows for pre-selected labels
            int label_p = models_indexes.at<int>(y,x);
            cv::Vec2f f_p = motion_labels[ label_p ].flow.at<cv::Vec2f>(y,x);

            cv::Point2f p_0_pf = cv::Point2f( float(x)+f_p[0], float(y)+f_p[1] );

            cv::Vec3f i_0_p = h_0.at<cv::Vec3f>(y,x);
            cv::Vec3f i_1_p = getSubpixColor( h_1, p_0_pf );
            double data_cost;

            if ( p_0_pf.x<0 || p_0_pf.x>=i_0.cols || p_0_pf.y<0 || p_0_pf.y>=i_0.rows )
                data_cost = 1000.0; //Just set an arbitrary high cost when the flow vectors fall outside the image
            else
                data_cost = cv::norm( i_0_p - i_1_p, cv::NORM_L2 );

            //std::cout<<data_cost<<std::endl;

            graph.setDataCost( p, 0, data_cost ); //VISIBLE
            graph.setDataCost( p, 1, epsilon ); //NOT-VISIBLE
            ++p;
        }
    }

    // Set neighborhood and weights (image dependent)
    {
    int q = 0;
    int p = 0;
    for (int y=0; y<i_0.rows; ++y) {
        for (int x=0; x<i_0.cols; ++x) {
            // Neighboors
            double weight_neighboors = 1.0;
            if ( x>0 ) { //left
                q = p-1;
                weight_neighboors = std::exp( -beta_o * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y,x-1), cv::NORM_L2 ) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            if ( x>0 && y>0 ) { //upleft
                q = p-1-i_0.cols;
                weight_neighboors = std::exp( -beta_o * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x-1), cv::NORM_L2 ) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            if ( y>0 ) { //up
                q = p-i_0.cols;
                weight_neighboors = std::exp( -beta_o * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x), cv::NORM_L2 ) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            if ( x<i_0.cols-1 && y>0 ) { //upright
                q = p-i_0.cols+1;
                weight_neighboors = std::exp( -beta_o * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x+1), cv::NORM_L2 ) );
                graph.setNeighbors(p,q,weight_neighboors);
            }

            p++;
        }
    }
    }

    // Build Label (O) cost matrix (and no label costs) ==> POTTS MODEL
    for ( int label1=0; label1<num_occ_labels; ++label1 ) {
        for ( int label2=0; label2<num_occ_labels; ++label2 ) {
            if (label1!=label2) {
                graph.setSmoothCost(label1,label2,coef_w);
            }else {
                graph.setSmoothCost(label1,label2,0.0);
            }
        }
    }

    // Initialize with previously computed occlusions
    if ( !occlusion_map.empty() ) {
        int p = 0;
        for (int y=0; y<i_0.rows; ++y) {
            for (int x=0; x<i_0.cols; ++x) {
                graph.setLabel( p, occlusion_map.at<uchar>(y,x) );
            }
        }
    }

    graph.setVerbosity(1);

    //Minimize with expansions
    graph.expansion(100);

    //Get labels after minimization of the energy
    {
        int p = 0;
        for (int y=0; y<i_0.rows; ++y) {
            for (int x=0; x<i_0.cols; ++x) {
                occlusion_map.at<uchar>(y,x) = (uchar)graph.whatLabel(p);
                ++p;
            }
        }
    }
    occlusions = occlusion_map.clone();
    //cv::imshow("occ2",255*occlusion_map);
    //cv::waitKey(0);
}

void OcclusionSegmentation::compute_occlusionfixed(cv::Mat& fusedIndexes, cv::Mat &flowVectors,
        double mu, double coef_l, double beta_l, double label_cost ) {

    //cv::imshow("occ1",255*occlusion_map);

    CV_Assert( motion_labels.size()>0 );
    fusedIndexes = models_indexes.clone();
    flowVectors = cv::Mat::zeros( fusedIndexes.size(), CV_32FC2 );


    if ( motion_labels.size()>1 ) {

        GCoptimizationGeneralGraph graph( h_0.rows * h_0.cols, motion_labels.size() );

        for ( int label=0; label<motion_labels.size(); ++label ) {


            // Flow map and plausability are for the label
            cv::Mat flow_l = motion_labels[ label ].flow;
            cv::Rect win = motion_labels[ label ].w;

            // For each point p=(x,y) and label, compose E_D(label_p)
            int p = 0;
            for (int y=0; y<i_0.rows; ++y) {
                for (int x=0; x<i_0.cols; ++x) {

                    double data_cost = 1.0; //

                    if (occlusion_map.at<uchar>(y,x)==0) { // Point is visible
                        // Auxiliary computations to find data term
                        cv::Vec2f f_p = flow_l.at<cv::Vec2f>(y,x);
                        cv::Vec3f i_0_p = h_0.at<cv::Vec3f>(y,x);
                        cv::Vec3f i_1_p = getSubpixColor( h_1, cv::Point2f( float(x)+f_p[0], float(y)+f_p[1] ) );
                        double raw_data_cost = cv::norm( i_0_p - i_1_p, cv::NORM_L2SQR );
                        data_cost = MIN(raw_data_cost,mu);
                        //data_cost = data_cost / ( data_cost + mu*mu );
                    }

                    if ( !isPointInWindow(x,y,win) ) {
                        data_cost *= 1.5;
                    }
                    graph.setDataCost( p, label, data_cost );
                    ++p;
                }
            }
        }

        {
        int q = 0;
        int p = 0;
        for (int y=0; y<i_0.rows; ++y) {
            for (int x=0; x<i_0.cols; ++x) {
                // Neighboors
                double weight_neighboors = 1.0;
                if ( x>0 ) { //left
                    q = p-1;
                    weight_neighboors = std::exp( -beta_l * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y,x-1), cv::NORM_L2 ) );
                    graph.setNeighbors(p,q,weight_neighboors);
                }

                if ( x>0 && y>0 ) { //upleft
                    q = p-1-i_0.cols;
                    weight_neighboors = std::exp( -beta_l * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x-1), cv::NORM_L2 ) );
                    graph.setNeighbors(p,q,weight_neighboors);
                }

                if ( y>0 ) { //up
                    q = p-i_0.cols;
                    weight_neighboors = std::exp( -beta_l * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x), cv::NORM_L2 ) );
                    graph.setNeighbors(p,q,weight_neighboors);
                }

                if ( x<i_0.cols-1 && y>0 ) { //upright
                    q = p-i_0.cols+1;
                    weight_neighboors = std::exp( -beta_l * cv::norm( i_0.at<cv::Vec3b>(y,x) - i_0.at<cv::Vec3b>(y-1,x+1), cv::NORM_L2 ) );
                    graph.setNeighbors(p,q,weight_neighboors);
                }

                p++;
            }
        }
        }

        float label_costs[motion_labels.size()];
        for ( int label1=0; label1<motion_labels.size(); ++label1 ) {

            label_costs[label1] = label_cost; // only one cost for now (not verifying belonging to windows)

            // Build label cost matrix
            for ( int label2=0; label2<motion_labels.size(); ++label2 ) {
                if (label1!=label2) {
                    graph.setSmoothCost(label1,label2,coef_l);
                }else {
                    graph.setSmoothCost(label1,label2,0.0);
                }
            }
        }

        // Initialize with previously computed labels
        if ( !models_indexes.empty() ) {
            int p = 0;
            for (int y=0; y<i_0.rows; ++y) {
                for (int x=0; x<i_0.cols; ++x) {
                    graph.setLabel( p, models_indexes.at<int>(y,x) );
                }
            }
        }

        graph.setLabelCost( label_costs );
        graph.setVerbosity(1);

        //Minimize with expansions
        graph.expansion(100);

        //Get labels and vector flows after minimization of the energy
        {
            int p = 0;
            for (int y=0; y<i_0.rows; ++y) {
                for (int x=0; x<i_0.cols; ++x) {
                      fusedIndexes.at<int>(y,x) = graph.whatLabel(p);
                      flowVectors.at<cv::Vec2f>(y,x) = motion_labels[ graph.whatLabel(p) ].flow.at<cv::Vec2f>(y,x);
                    ++p;
                }
            }
        }

    }



    // Update member copy
    models_indexes = fusedIndexes.clone();

}


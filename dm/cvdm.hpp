/*
Copyright (C) 2015 Juan-Manuel Perez-Rua
*/
#ifndef ___CVDM_H___
#define ___CVDM_H___

#include <opencv2/core.hpp>
#include <vector>

class DeepMatch
{
public:
    /*!
     * \brief DeepMatch::DeepMatch
     */
    DeepMatch ();

    /*!
     * \brief DeepMatch::DeepMatch
     * \param prior_img_downscale
     * \param overlap
     * \param subsample_ref
     * \param nlpow
     * \param maxima_node
     * \param low_mem
     * \param min_level
     * \param scoring_mode
     */
    DeepMatch (
        int prior_img_downscale,
        int overlap,
        int subsample_ref,
        double nlpow,
        int maxima_node,
        int low_mem,
        int min_level,
        int scoring_mode
    );

    /*!
     * \brief DeepMatch::compute
     * \param pts1
     * \param pts2
     * \param n_thread
     * \param verbose_mode
     */
    void compute (
            const cv::Mat& im1,
            const cv::Mat& im2,
            std::vector<cv::Point>& pts1,
            std::vector<cv::Point>& pts2,
            int n_thread = 1,
            int verbose_mode = 0
        );

private:
    int m_prior_img_downscale;
    int m_overlap;
    int m_subsample_ref;
    double m_nlpow;
    int m_maxima_node;
    int m_low_mem;
    int m_min_level;
    int m_scoring_mode;
};

#endif

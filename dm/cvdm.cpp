/*
Copyright (C) 2014 Juan-Manuel Perez-Rua

*/
#include "cvdm.hpp"

#include <opencv2/imgproc.hpp>

#include "std.h"
#include "image.h"
#include "deep_matching.h"

/*!
 * \brief DeepMatch::DeepMatch
 * \param prior_img_downscale
 * \param overlap
 * \param subsample_ref
 * \param nlpow
 * \param maxima_node
 * \param low_mem
 * \param min_level
 * \param scoring_mode
 */
DeepMatch::DeepMatch(
        int prior_img_downscale,
        int overlap,
        int subsample_ref,
        double nlpow,
        int maxima_node,
        int low_mem,
        int min_level,
        int scoring_mode) {

    m_prior_img_downscale = prior_img_downscale;
    m_overlap = overlap;
    m_subsample_ref = subsample_ref;
    m_nlpow = nlpow;
    m_maxima_node = maxima_node;
    m_low_mem = low_mem;
    m_min_level = min_level;
    m_scoring_mode = scoring_mode;
}

/*!
 * \brief DeepMatch::DeepMatch
 */
DeepMatch::DeepMatch(){
    m_prior_img_downscale = 2;
    m_overlap = 0;
    m_subsample_ref = 1;
    m_nlpow = 1.6;
    m_maxima_node = 1;
    m_low_mem = 0;
    m_min_level = 2;
    m_scoring_mode = 0;
}

/* convert a color Mat to a gray-scale image */
static inline
image_t* image_gray_from_mat( const cv::Mat& img ) {
  cv::Mat _img;
  if ( img.type() == CV_8UC3 )
      cv::cvtColor( img, _img, cv::COLOR_BGR2GRAY );
  else
      _img = img;

  image_t* res = image_new(img.cols, img.rows);

  for(int j=0; j<img.rows; j++)
    for(int i=0; i<img.cols; i++)
    res->data[i+j*res->stride] = float(_img.at<uchar>(j,i));

  return res;
}

/* Create vectors of points from corrs */
static inline
void points_from_correspondences( const corres_t* corres, int nb, float fx, float fy,
                                  std::vector<cv::Point>& pts1,
                                  std::vector<cv::Point>& pts2
                                 )
{
  assert(0<fx && fx<=2);
  assert(0<fy && fy<=2);

  pts1.clear();
  pts2.clear();

  for(int i=0; i<nb; i++) {
    const corres_t* r = corres + i; // one row

    pts1.push_back( cv::Point( fx*r->x0, fy*r->y0 ) );
    pts2.push_back( cv::Point( fx*r->x1, fy*r->y1 ) );
  }
}

void DeepMatch::compute (
        const cv::Mat &_im1, const cv::Mat &_im2,
        std::vector<cv::Point> &pts1,
        std::vector<cv::Point> &pts2,
        int n_thread,
        int verbose_mode) {

    // Images have to be grayscale
    image_t *im1=NULL, *im2=NULL;
    im1 = image_gray_from_mat( _im1 );
    im2 = image_gray_from_mat( _im2 );

    // Params
    dm_params_t params;
    params.prior_img_downscale = m_prior_img_downscale;
    params.overlap = m_overlap;
    params.subsample_ref = m_subsample_ref;
    params.nlpow = m_nlpow;
    params.maxima_mode = m_maxima_node;
    params.low_mem = m_low_mem;
    params.min_level = m_min_level;
    params.scoring_mode = m_scoring_mode;
    params.desc_params.presmooth_sigma = 0; // no image smoothing
    params.desc_params.hog_sigmoid = 0.2;
    params.desc_params.mid_smoothing = 1.5;
    params.desc_params.post_smoothing = 1;
    params.desc_params.ninth_dim = 0.1; // low ninth_dim since image PSNR is high

    params.n_thread = n_thread;
    params.verbose = verbose_mode;

    // compute deep matching
    float fx=1, fy=1;
    float_image* corres = deep_matching( im1, im2, &params ); // This fails for very large images, jum..
    points_from_correspondences( (corres_t*)corres->pixels, corres->ty, fx, fy, pts1, pts2 );

    // free memory
    free_image(corres);
    image_delete(im1);
    image_delete(im2);
}

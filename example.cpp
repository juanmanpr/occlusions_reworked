
#include "flow/flow_cv.h"
#include "flow/colorcode.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/optflow.hpp>
#include "occlusion_segmentation/occseg_no_motion2d.hpp"

using namespace std;
using namespace cv;

const char* keys = {
    "{@0|./frame_0012.png         | first image    }"
    "{@1|./frame_0013.png         | second image   }"
    "{@2|./out.jpg                | output name    }"
    "{ff|                         | forward flow   }"
    "{bf|                         | backward flow  }"
    "{mo|./masked_out.jpg         | masked output  }"
    "{beta_rgb  |0.5              | RGB parameter  }"
    "{beta_flo  |10.0             | Flow parameter }"
    "{beta_gmm  |0.1              | GMM parameter  }"
    "{epsilon   |0.50             | Threshold parameter  }"
    "{alpha_rgb |0.10             | Color-based regularization parameter  }"
    "{alpha_gra |0.10             | Gradient-based regularization parameter }"
    "{potts     |0.10             | Potts coeff }"
};


int main( int argc, const char** argv ) {

    // PARSE INPUT
    CommandLineParser parser(argc, argv, keys);

    string im1_name = parser.get<string>(0);
    string im2_name = parser.get<string>(1);
    string out_name = parser.get<string>(2);
    string msk_name = parser.get<string>("mo");
    string ff_name  = parser.get<string>("ff");
    string bf_name  = parser.get<string>("bf");
    float beta_rgb  = parser.get<float>("beta_rgb");
    float beta_flo  = parser.get<float>("beta_flo");
    float beta_gmm  = parser.get<float>("beta_gmm");
    float epsilon   = parser.get<float>("epsilon");
    float alpha_rgb = parser.get<float>("alpha_rgb");
    float alpha_gra = parser.get<float>("alpha_gra");
    float potts     = parser.get<float>("potts");

    cv::Mat im1 = cv::imread(im1_name, cv::IMREAD_COLOR);
    cv::Mat im2 = cv::imread(im2_name, cv::IMREAD_COLOR);
    cv::Mat flow_f, flow_b;

    std::cout<<"Proccessing starting..."<<std::endl;
    std::cout<<im1_name<<": "<<"("<<im1.rows<<","<<im1.cols<<")"<<std::endl;
    std::cout<<out_name<<std::endl;

    if (im1.empty() || im2.empty()){
        std::cerr<<"Images could not be opened, the paths were: "<<std::endl;
        std::cerr<<"I1: "<<im1_name<<std::endl;
        std::cerr<<"I2: "<<im2_name<<std::endl;
        return -1;
    }

    if (!ff_name.empty()) {
        readFlowFile(flow_f, ff_name);
    }else{
        std::cout<<"Forward Flow not provided"<<std::endl;
    }

    if (!bf_name.empty()) {
        readFlowFile(flow_b, bf_name);
    }else{
        std::cout<<"Backward Flow not provided"<<std::endl;
    }

    std::cout<<" *** Initializing ... "<<std::endl;
    double exec_time = (double)cv::getTickCount();
    OcclusionSegmentation::Params params(beta_gmm, beta_flo, beta_rgb, epsilon, alpha_rgb, alpha_gra, potts);
    OcclusionSegmentation occ_machine(params);
    cv::Mat occlusions;
    exec_time = ((double)getTickCount() - exec_time)/getTickFrequency();
    std::cout<<" Initialization Time = "<<exec_time<< " s. "<<std::endl;
    exec_time = (double)cv::getTickCount();
    occ_machine.compute(im1, im2, flow_f, flow_b);
    occlusions = occ_machine.getOcclusions();
    exec_time = ((double)getTickCount() - exec_time)/getTickFrequency();
    std::cout<<" Execution Time = "<<exec_time<< " s. "<<std::endl;
    cv::imwrite(out_name, occlusions);
    cv::Mat masked, cocc;
    cv::cvtColor(occlusions, cocc, CV_GRAY2BGR);
    cv::multiply(im1, (255-cocc)/255, masked);
    cv::imwrite(out_name, occlusions);
    cv::imwrite(msk_name, masked);

    return 0;
}

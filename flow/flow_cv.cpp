#include "flow_cv.h"

static void mat2image(const cv::Mat& matcv, CFloatImage& image ) {
    CShape shape(matcv.cols, matcv.rows, 2);
    image.ReAllocate(shape);

    for (int y=0; y<matcv.rows; ++y) {
        const float* mat_p = matcv.ptr<float>(y);
        int matx=0;
        for (int x=0; x<matcv.cols; ++x) {
            image.Pixel(x,y,0) = mat_p[matx++];
            image.Pixel(x,y,1) = mat_p[matx++];
        }
    }
}

void readFlowFile(cv::Mat& flow, string filename) {
    CFloatImage cflow;
    ReadFlowFile(cflow, filename.c_str());
    CShape shape = cflow.Shape();

    flow.create(shape.height, shape.width, CV_32FC2);

    for (int y=0; y<flow.rows; ++y) {
        float* matflow_p = flow.ptr<float>(y);
        int matx = 0;
        for (int x=0; x<flow.cols; ++x) {
            matflow_p[matx++] = cflow.Pixel(x,y,0);
            matflow_p[matx++] = cflow.Pixel(x,y,1);
        }
    }
}

void getOcclusionsFromFlow( const cv::Mat& flow, cv::Mat& occl ) {
    occl = cv::Mat::zeros(flow.size(),CV_8UC1);
    for (int row=0; row<occl.rows; ++row) {
        for (int col=0; col<occl.cols; ++col) {
            if ( flow.at<cv::Vec2f>(row,col)[0]>=UNKNOWN_FLOW_THRESH ||
                 flow.at<cv::Vec2f>(row,col)[1]>=UNKNOWN_FLOW_THRESH ) {
                occl.at<uchar>(row,col)=255;
            }
        }
    }
}

#ifndef __COLORCODE_H__
#define __COLORCODE_H__

#include <stdlib.h>
#include <math.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <limits>

#define M_PI 3.14159265358979323846

void computeColor(float fx, float fy, cv::Scalar_<uchar> &pix);
void getColorMat(const std::vector<cv::Mat> &flow, cv::Mat &colored);
void getColorMat(const cv::Mat &flow, cv::Mat &colored);
void getColorMat(const std::vector<cv::Mat> &flow, cv::Mat &colored, float reference);
void getColorMat(const cv::Mat &flow, cv::Mat &colored, float reference);
#endif

// flow_cv.h

#ifndef FLOW_CV_H
#define FLOW_CV_H

#include "flowIO.h"
#include "imageLib/Image.h"
#include <opencv2/core.hpp>
#include <string>

// read a flow file into 2-band image
void readFlowFile(cv::Mat& flow, string filename);

void getOcclusionsFromFlow( const cv::Mat& flow, cv::Mat& occl );

#endif
